
- Features: Implement monde-diplo-monthly ([#15](https://framagit.org/parisni/podcast-dl/-/issues/15))


- Bugfixes: Fix cli output path ([#14](https://framagit.org/parisni/podcast-dl/-/issues/14))


- Bugfixes: Various bug fixes ([#13](https://framagit.org/parisni/podcast-dl/-/issues/13))


- Features: Use france culture api ([#12](https://framagit.org/parisni/podcast-dl/-/issues/12))


- Features: Add logging ([#11](https://framagit.org/parisni/podcast-dl/-/issues/11))


- Features: Extract series into album when possible ([#10](https://framagit.org/parisni/podcast-dl/-/issues/10))


- Features: Add download and tag feature ([#8](https://framagit.org/parisni/podcast-dl/-/issues/8))


- Bugfixes: Fix missing url in some emission ([#7](https://framagit.org/parisni/podcast-dl/-/issues/7))


- Features: Add support for pages number and csv export ([#6](https://framagit.org/parisni/podcast-dl/-/issues/6))


v0.1.0 - 2021-08-18
- Features: Release on pypi ([#2](https://framagit.org/parisni/podcast-dl/-/issues/2))
- Features: Create a python binary: podcast-scraper ([#3](https://framagit.org/parisni/podcast-dl/-/issues/3))
- Features: Rename to podcast-scraper ([#4](https://framagit.org/parisni/podcast-dl/-/issues/4))
- Features: Provide le Monde Diplo ([#5](https://framagit.org/parisni/podcast-dl/-/issues/5))
