from pathlib import Path

from podcast_scraper.csv_manager import CsvManager
from podcast_scraper.france_culture import FranceCulture

URL = "https://www.radiofrance.fr/franceculture/podcasts/carbone-14-le-magazine-de-l-archeologie"
URL = "serie-daniel-zagury"
URL = "https://www.radiofrance.fr/franceculture/podcasts/carbone-14-le-magazine-de-l-archeologie"
URL = "https://www.franceculture.fr/emissions/fictions-le-feuilleton"
MAIN_CONTENT = (Path(__file__).parent / "resources" / "france-culture.html").read_text()
CSV_PATH = Path(__file__).parent / "resources" / "france-culture.csv"


def test_get_url_content():
    print(FranceCulture(URL).print_content(page_number=-1))


def test_download_content(tmpdir):
    pdw = CsvManager()
    pdw.with_csv(CSV_PATH).with_output(tmpdir).with_limit(1).replace_tags()
