from pathlib import Path

from podcast_scraper.monde_diplo import MondeDiplo

URL = "https://www.monde-diplomatique.fr/audio?debut_sons=820#pagination_sons"
MAIN_CONTENT = (Path(__file__).parent / "resources" / "monde-diplo.html").read_text()


def test_extract_url():
    result = MondeDiplo().get_urls(content=MAIN_CONTENT)
    assert len(result["url"]) == 20


def test_get_url_content():
    MondeDiplo().print_urls(URL, 1)
