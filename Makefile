.DEFAULT_GOAL := help
.PHONY: help docs format start stop

help:   ## Podcast-scraper commands
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

release.patch: ## stop the local docker stack
	git fetch --force --tags
	bump2version --allow-dirty patch
	towncrier build --yes --version $$(bump2version --allow-dirty --dry-run --list patch | grep current_version | sed -r s,"^.*=",,)
	release-it -i patch

release.minor: ## stop the local docker stack
	git fetch --force --tags
	bump2version --allow-dirty minor
	towncrier build --yes --version $$(bump2version --allow-dirty --dry-run --list patch | grep current_version | sed -r s,"^.*=",,)
	release-it minor

release.major: ## stop the local docker stack
	git fetch --force --tags
	bump2version --allow-dirty major
	towncrier build --yes --version $$(bump2version --allow-dirty --dry-run --list patch | grep current_version | sed -r s,"^.*=",,)
	release-it major

upload.pypi: clean ## upload on pypi
	python setup.py sdist
	twine upload dist/*

clean:
	rm -rf dist/ build/ podcast_scraper.egg-info/
